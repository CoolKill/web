﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewsConnection;

namespace WebApplication1.Models.Bus
{
    public class ContentBus
    {
        public static IEnumerable<Content> DanhSach()
        {
            var db = new NewsConnectionDB();
            return db.Query<Content>("select * from Header");
        }
        public static Content ChiTiet(int id)
        {
            var db = new NewsConnectionDB();
            return db.SingleOrDefault<Content>("select * from  Content where idHeader = @0", id);
        }
        public static void Them(int id,Content ct)
        {
            var db = new NewsConnectionDB();
            ct.idHeader = id;
            db.Insert(ct);
        }

        public static void Xoa(int id)
        {
            var db = new NewsConnectionDB();
            db.Delete<Content>("WHERE idHeader=@0", id);
        }
    }
}