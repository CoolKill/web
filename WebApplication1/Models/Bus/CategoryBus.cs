﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewsConnection;

namespace WebApplication1.Models.Bus
{
    public class CategoryBus
    {
        public static IEnumerable<Category> DanhSach()
        {
            var db = new NewsConnectionDB();
            return db.Query<Category>("select * from Category");
        }
        public static IEnumerable<Header> ChiTiet(int id)
        {
            var db = new NewsConnectionDB();
            return db.Query<Header>("select * from Header where idCategory = @0",id);
        }
        public static Category ChiTietCat(int id)
        {
            var db = new NewsConnectionDB();
            return db.SingleOrDefault<Category>("select * from  Category where id = @0", id);
        }

        public static void Them(Category cat)
        {
            var db = new NewsConnectionDB();
            db.Insert(cat);
        }
        public static void Sua(int id, Category cat)
        {
            var db = new NewsConnectionDB();
            db.Update(cat);
        }
        public static void Xoa(int id)
        {
            var db = new NewsConnectionDB();
            db.Delete<Category>("WHERE id=@0", id);
        }
    }
}