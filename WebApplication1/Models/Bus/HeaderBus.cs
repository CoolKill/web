﻿using NewsConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Bus
{
    public class HeaderBus
    {
        public static IEnumerable<Header> DanhSach()
        {
            var db = new NewsConnectionDB();
            return db.Query<Header>("select * from Header");
        }
        public static Header ChiTiet(int id)
        {
            var db = new NewsConnectionDB();
            return db.SingleOrDefault<Header>("select * from  Header where id = @0", id);
        }
        public static void Them(Header hd)
        {
            var db = new NewsConnectionDB();
            db.Insert(hd);
        }
        public static void Sua(int id, Header hd)
        {
            var db = new NewsConnectionDB();
            db.Update(hd);
        }
        public static void Xoa(int id)
        {
            var db = new NewsConnectionDB();
            db.Delete<Header>("WHERE id=@0", id);
        }
    }
}