﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models.Bus;
using PagedList;
using PagedList.Mvc;
namespace WebApplication1.Controllers

{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index(int id, int page = 1, int pagesize = 2)
        {
            var ds = CategoryBus.ChiTiet(id).ToPagedList(page, pagesize);
            return View(ds);
        }
    }
}