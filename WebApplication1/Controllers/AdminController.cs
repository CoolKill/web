﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsConnection;
using WebApplication1.Models.Bus;
using PagedList;
using PagedList.Mvc;


namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index(int page = 1, int pagesize = 10)
        {
            var dsHeader = HeaderBus.DanhSach().ToPagedList(page, pagesize);
            return View(dsHeader);
        }
        public ActionResult IndexCat(int page = 1, int pagesize = 10)
        {
            var dsCat = CategoryBus.DanhSach().ToPagedList(page, pagesize);
            return View(dsCat);
        }


        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(Header hd,Content ct,HttpPostedFileBase file)
        {
            if (file != null)
            {
                string ImageName = System.IO.Path.GetFileName(file.FileName);
                string physicalPath = Server.MapPath("/images/headers/" + ImageName);
                file.SaveAs(physicalPath);
                hd.image = "/images/headers/" + ImageName;
            }
            HeaderBus.Them(hd);
            ContentBus.Them(hd.id, ct);
            return RedirectToAction("Index");
        }
        public ActionResult CreateCat()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateCat(Category cat)
        {
            CategoryBus.Them(cat);
            return RedirectToAction("IndexCat");
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View(HeaderBus.ChiTiet(id));
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Header hd, HttpPostedFileBase file)
        {
            if (file != null)
            {
                string ImageName = System.IO.Path.GetFileName(file.FileName);
                string physicalPath = Server.MapPath("/images/headers/" + ImageName);
                file.SaveAs(physicalPath);
                hd.image = "/images/headers/" + ImageName;
            }
                HeaderBus.Sua(id, hd);
                return RedirectToAction("Index");
            
            

        }
        public ActionResult EditCat(int id)
        {
            return View(CategoryBus.ChiTietCat(id));
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult EditCat(int id, Category cat)
        {
            try
            {
                // TODO: Add update logic here
                CategoryBus.Sua(id,cat);
                return RedirectToAction("IndexCat");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View(HeaderBus.ChiTiet(id));
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id,Header hd)
        {
                ContentBus.Xoa(id);
                HeaderBus.Xoa(id);
                return RedirectToAction("Index"); 
        }

        public ActionResult DeleteCat(int id)
        {
            return View(CategoryBus.ChiTietCat(id));
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult DeleteCat(int id, Header hd)
        {
            CategoryBus.Xoa(id);
            return RedirectToAction("IndexCat");
        }
    }
}
