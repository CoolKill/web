﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models.Bus;
using PagedList;
using PagedList.Mvc;

namespace WebApplication1.Controllers
{
    public class HeaderController : Controller
    {
        // GET: Header
        public ActionResult Index(int page = 1, int pagesize = 2)
        {
            var dsHeader = HeaderBus.DanhSach().ToPagedList(page,pagesize);
            return View(dsHeader);
        }

        // GET: Header/Details/5
        public ActionResult Details(int id)
        {
            var db = HeaderBus.ChiTiet(id);
            return View(db);
        }

        // GET: Header/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Header/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Header/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Header/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Header/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Header/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
