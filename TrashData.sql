USE [master]
GO
/****** Object:  Database [news]    Script Date: 04/25/2017 17:12:27 ******/
CREATE DATABASE [news] ON  PRIMARY 
( NAME = N'news', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\news.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'news_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\news_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [news] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [news].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [news] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [news] SET ANSI_NULLS OFF
GO
ALTER DATABASE [news] SET ANSI_PADDING OFF
GO
ALTER DATABASE [news] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [news] SET ARITHABORT OFF
GO
ALTER DATABASE [news] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [news] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [news] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [news] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [news] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [news] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [news] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [news] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [news] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [news] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [news] SET  DISABLE_BROKER
GO
ALTER DATABASE [news] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [news] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [news] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [news] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [news] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [news] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [news] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [news] SET  READ_WRITE
GO
ALTER DATABASE [news] SET RECOVERY SIMPLE
GO
ALTER DATABASE [news] SET  MULTI_USER
GO
ALTER DATABASE [news] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [news] SET DB_CHAINING OFF
GO
USE [news]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 04/25/2017 17:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NCategory] [nvarchar](50) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON
INSERT [dbo].[Category] ([id], [NCategory]) VALUES (1, N'Xã hội')
INSERT [dbo].[Category] ([id], [NCategory]) VALUES (2, N'Pháp luật')
INSERT [dbo].[Category] ([id], [NCategory]) VALUES (3, N'Kinh tế')
INSERT [dbo].[Category] ([id], [NCategory]) VALUES (4, N'Thế giới')
INSERT [dbo].[Category] ([id], [NCategory]) VALUES (5, N'Blablablas')
SET IDENTITY_INSERT [dbo].[Category] OFF
/****** Object:  Table [dbo].[Header]    Script Date: 04/25/2017 17:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Header](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NHeader] [nvarchar](max) NULL,
	[NDesc] [nvarchar](max) NULL,
	[NDate] [date] NULL,
	[UserName] [nvarchar](max) NULL,
	[idCategory] [int] NULL,
	[image] [nvarchar](max) NULL,
 CONSTRAINT [PK_Header] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Header] ON
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (1, N'Bắt giữ ai đó', N'Mới bắt dc ai đó', CAST(0x493C0B00 AS Date), N'asd       ', 1, N'/images/featured_img1.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (2, N'Blahblah', N'Thế giới mới xảy ra gì đó', CAST(0x693C0B00 AS Date), N'dsa       ', 4, N'/images/featured_img2.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (35, N'dasdsa', NULL, NULL, NULL, 1, N'/images/headers/4dFKMsk.png')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (36, N'My Job Was Telling Bush Things He Didn’t Want To Hear', N'This is what happens when you have an intellectual throw-down with the most powerful man on earth.', CAST(0xBA3C0B00 AS Date), N'Admin', 1, N'/images/headers/608629_v2.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (37, N'The Terrifying Things You Learn In Military Intelligence', N'Here''s what we learned about a profession in which you literally decide who lives and who dies.', NULL, N'Admin', 1, N'/images/headers/608137_v2.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (38, N'Why Everything You Know About Vikings Is A Lie', N'Being a viking was less an awesome, bloody lifestyle, and more like ''this is a 9-to-5 that might actually kill me', CAST(0xB93C0B00 AS Date), NULL, 2, N'/images/headers/viking-warrior-symbols-wallpaper-2.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (39, N'How Hitler Ruined Our Ability To Discuss Politics', N'We really need to stop using Hitler as some sort of benchmark.', NULL, NULL, 2, N'/images/headers/608459_v2.jpg')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (40, N'Read This And Never Watch A ''Fast And Furious'' Movie Again', N'Because people will watch anything that features Ludacris traveling at over 100 miles per hour, here''s a handy guide to a series that I love.', NULL, NULL, 2, N'/images/headers/609182_v2')
INSERT [dbo].[Header] ([id], [NHeader], [NDesc], [NDate], [UserName], [idCategory], [image]) VALUES (41, N'Náo loạn ở buổi xin lỗi công khai người 4 lần bị tuyên án tử', N'Ít phút trước khi cơ quan chức năng xin lỗi công khai người từng bị tuyên án tử, gia đình bé gái bị sát hại hơn chục năm trước đã gây náo loạn ở hội trường.', CAST(0xBB3C0B00 AS Date), NULL, 3, N'/images/headers/naoloanoBacGiang.jpg')
SET IDENTITY_INSERT [dbo].[Header] OFF
/****** Object:  Table [dbo].[Status]    Script Date: 04/25/2017 17:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[idHeader] [int] NULL,
	[NStatus] [bit] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Status] ([idHeader], [NStatus]) VALUES (1, 1)
INSERT [dbo].[Status] ([idHeader], [NStatus]) VALUES (2, 0)
/****** Object:  Table [dbo].[Content]    Script Date: 04/25/2017 17:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Content](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idHeader] [int] NULL,
	[NContent] [ntext] NULL,
	[image1] [nvarchar](max) NULL,
	[image2] [nvarchar](max) NULL,
	[image3] [nvarchar](max) NULL,
	[image4] [nvarchar](max) NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Content] ON
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (1, 1, N'vào sáng cùng ngày bắt giữ nghi phạm ở đâu đó ở việt nam sau r` cho vào tù hay gì đó tương tự, nói thật cái này dài quá lười viết', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (2, 2, N'hiện tại không biết tận thế là khi nào nhưng mà đoán là chắc ngày mai tận thế, theo ý kiến của 1 chuyên gia NASA ẩn danh cho hay.', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (7, 35, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (8, 36, N'Once upon a time, John Nixon was a senior CIA leadership analyst. When Saddam Hussein was captured, John was the man the agency picked to interrogate him (he wrote a book about that). And afterward, he briefed President George W. Bush about it on two separate occasions. One of those visits went well. One did not. John has the relatively rare experience of having to tell a sitting U.S. president something he did not want to hear. That''s getting to be a slightly more common experience these days, so we figured, "Hey, let''s ask John what that''s like, rather than simply asking him a bunch of questions about how Saddam Hussein smelled." (Bad, we assume.) Here''s what he told us about having an intellectual throwdown with the most powerful man on earth.', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (9, 37, N'Intelligence specialists translate and analyze communications between America''s enemies, then let us know which one of them desperately needs a Hellfire missile up the ass. We''re talking about a profession in which you literally decide who lives and who dies. That''s pressure. We wanted to learn more about the folks who deal with that kind of responsibility on a daily basis, while we freak out at the psychic weight of caring for a houseplant. So we spoke to Gwen, Greg, and Eddie. This is what they told us:', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (10, 38, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (11, 39, N'Sean Spicer, White House press secretary and mound of Arby''s roast beef given hateful sentience by an unholy flesh magician, recently made the news for somehow bungling a press conference meant to denounce the actions of a brutal dictator with a comparison that probably gave Mike Godwin an aneurysm.', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (12, 40, N'James Bond. Star Wars. The Lord Of The Rings. The Fast And The Furious. Three of these series are respected franchises that people will openly devote their passion to. The other is The Fast And The Furious -- which, by the way, just garnered the largest international opening weekend box office in the history of both cars and movies. And now, rather than scorn or derision, many react to the success of these films with "Why? Why are people seeing these things? What is this reality?"', NULL, NULL, NULL, NULL)
INSERT [dbo].[Content] ([id], [idHeader], [NContent], [image1], [image2], [image3], [image4]) VALUES (13, 41, N'Theo dự kiến, 14h ngày 25/4, TAND Cấp cao tại Hà Nội tổ chức buổi công khai xin lỗi ông Hàn Đức Long (58 tuổi, ở huyện Tân Yên, Bắc Giang) tại trụ sở UBND xã Phúc Sơn, huyện Tân Yên. Hơn chục phút trước đó, ông Long cùng khoảng 20 người thân trong gia đình đến dự.', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Content] OFF
/****** Object:  ForeignKey [FK_Header_Category]    Script Date: 04/25/2017 17:12:28 ******/
ALTER TABLE [dbo].[Header]  WITH CHECK ADD  CONSTRAINT [FK_Header_Category] FOREIGN KEY([idCategory])
REFERENCES [dbo].[Category] ([id])
GO
ALTER TABLE [dbo].[Header] CHECK CONSTRAINT [FK_Header_Category]
GO
/****** Object:  ForeignKey [FK_Status_Header]    Script Date: 04/25/2017 17:12:28 ******/
ALTER TABLE [dbo].[Status]  WITH CHECK ADD  CONSTRAINT [FK_Status_Header] FOREIGN KEY([idHeader])
REFERENCES [dbo].[Header] ([id])
GO
ALTER TABLE [dbo].[Status] CHECK CONSTRAINT [FK_Status_Header]
GO
/****** Object:  ForeignKey [FK_Content_Header]    Script Date: 04/25/2017 17:12:28 ******/
ALTER TABLE [dbo].[Content]  WITH CHECK ADD  CONSTRAINT [FK_Content_Header] FOREIGN KEY([idHeader])
REFERENCES [dbo].[Header] ([id])
GO
ALTER TABLE [dbo].[Content] CHECK CONSTRAINT [FK_Content_Header]
GO
